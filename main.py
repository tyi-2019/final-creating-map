import cv2 as cv
import numpy as np
from pyunpack import Archive
import zipfile
import os
import subprocess
import glob
import extractor_function as extractor
import math
import camera_parameters as vars
from random import randint

def angle_to_metres(lon, lat):
    t = []
    # print("###", lon * 1000)
    t.append(lon * 111321 * math.cos(lat * math.pi / 180))
    t.append(lat * 111134)  # metres
    return t

def get_dots(x, y, roll, pitch):
    print("")

def spin_dot_around_center(x0, y0, x, y, angle):
    x1 = x - x0
    y1 = y - y0
    x2 = x1 * math.cos(angle / 180 * math.pi) - y1 * math.sin(angle / 180 * math.pi)
    y2 = x1 * math.sin(angle / 180 * math.pi) + y1 * math.cos(angle / 180 * math.pi)
    x2 += x0
    y2 += y0
    ans = [x2, y2]
    return ans

def overflow(dots, background, img_idx):
    fout = open("dots.txt", "w")
    for i in range(0, 4):
        fout.write(str(int(dots[i][0])) + ' ' + str(int(dots[i][1])) + '\n')
    # print(dots)
    fout.close()
    args = ['./a.out', 'res.jpg', 'images/' + Photos[img_idx].lower()]
    subprocess.call(args)

    

def write_logs_to_json(filename):
    # subprocess.call('python3 -m mavlogdump logs.tlog > 222.txt', shell=True)
    print('python3 -m mavlogdump --format=json ' + filename + ' > logs.json')
    subprocess.call('python3 -m mavlogdump --format=json ' + filename + ' > logs.json', shell=True)
    # subprocess.call('python3 -m mavlogdump --format=csv --type=CAMERA_FEEDBACK logs.tlog > CSVQ.txt', shell=True)

def start():
    subprocess.call("rm -rf images", shell=True)
    subprocess.call("mkdir images", shell=True)

    Archive('photoset.zip').extractall('images')
    # return
    # os.chdir('./images')
    # Files = []
    # for file in glob.glob("*"):
    #     if (file == '__MACOSX'):
    #         continue
    #     Files.append(file)
    # os.chdir('../')
    # print(Files)
    # subprocess.call('mv ./' + Files[0] + '/* ./')
    # subprocess.call('rm -rf ./' + Files[0])
    # return
    Logs = []
    os.chdir('./images')
    for file in glob.glob("*.tlog"):
        Logs.append(file)
    os.chdir('../')
    print(Logs[0])
    write_logs_to_json('images/' + Logs[0])
    # logs = extractor.extract_json('logs.json', 'CAMERA_FEEDBACK')
    # print(logs)


start()
Photos = []
os.chdir('./images')
for file in glob.glob("*.JPG"):
    Photos.append(file)
Photos.sort()
os.chdir('../')
camera_feedback = extractor.extract_json("logs.json", "CAMERA_FEEDBACK")
# gps = extractor.extract("logs.txt", "mavlink_global_position_int_t")
Background = cv.imread('background.png')
cv.imwrite("res.jpg", Background)

start_x, start_y = camera_feedback[0]['data']['lng'], camera_feedback[0]['data']['lat']
start_x, start_y = float(start_x), float(start_y)
start_x /= 1e7
start_y /= 1e7
t = angle_to_metres(start_x, start_y)
start_x = t[0]
start_y = t[1]


min_x, min_y = 1e9, 1e9
max_x, max_y = 0, 0

min_height = 1000
for i in range(0, 130):
    photo = cv.imread('images/' + Photos[i].lower())
    original_photo = photo
    resized = cv.resize(photo, (int(min_height), int(photo.shape[0] * (min_height / photo.shape[1]))))
    # print("XYI SIZE:", resized.shape[0], resized.shape[1])
    cv.imwrite('images/' + Photos[i].lower(), resized)
    # continue
    # print('images/' + Photos[i].lower(), photo)
    info = camera_feedback[i]
    h = info['data']['alt_rel']
    h = abs(float(h))
    # x, y = get_gps_by_time(gps, info['time'])
    x, y = float(info['data']['lng']), float(info['data']['lat'])
    # x, y = int(x), int(y)
    # print("X AND Y:", x, y)
    x /= 1e7
    y /= 1e7
    
    t = angle_to_metres(x, y)
    x = t[0]
    y = t[1]

    x -= start_x
    y = start_y - y
    print("X AND Y:", x, y)
    dots = [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0]
    ]
    # continue
    now_res = cv.imread("res.jpg")
    if (abs(float(info['data']['pitch'])) < 4 and abs(float(info['data']['roll'])) < 4):
        # print("x and y in metres", x, y)
        print("STARTUEM")
        ph_height_in_m = 2 * h * math.tan(vars.vertical_angle / 360 * math.pi)
        ph_width_in_m = 2 * h * math.tan(vars.horizontal_angle / 360 * math.pi)
        dots[1] = [x - ph_width_in_m / 2, y + ph_height_in_m / 2]
        dots[2] = [x + ph_width_in_m / 2, y + ph_height_in_m / 2]
        dots[3] = [x + ph_width_in_m / 2, y - ph_height_in_m / 2]
        dots[0] = [x - ph_width_in_m / 2, y - ph_height_in_m / 2]
        # print("HEIGHT AND WIDTH IN METERS:", ph_height_in_m, ph_width_in_m)
        start_dot_x, start_dot_y = int(Background.shape[1] / 2), int(Background.shape[0] / 2)
        # zoom = 0.3
        ph_height_in_p, ph_width_in_p = photo.shape[0], photo.shape[1]
        
        # print("PHOTO SIZE:", ph_height_in_p, ph_width_in_p)
        # k = float((ph_width_in_m / ph_width_in_p) / zoom)
        k = 0.15
        # x0, y0 = x * k, y * k
        for j in range(0, 4):
            # dots[j][0], dots[j][1] = dots[j][0] * k, dots[j][1] * k 
            angle = float(info['data']['yaw'])
            dots[j] = spin_dot_around_center(x, y, dots[j][0], dots[j][1], angle)
            dots[j] = dots[j][0] * k + start_dot_x, dots[j][1] * k + start_dot_y
            min_x = min(min_x, dots[j][1])
            max_x = max(max_x, dots[j][1])
            min_y = min(min_y, dots[j][0])
            max_y = max(max_y, dots[j][0])
        overflow(dots, now_res, i)


img = cv.imread("res.jpg")
img = img[int(min_x):int(max_x), int(min_y):int(max_y)]
img = cv.resize(img, (int(img.shape[1] * 2), int(img.shape[0] * 2)), interpolation=cv.INTER_LINEAR)
cv.imwrite("res.jpg", img)



